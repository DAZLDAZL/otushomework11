import React, { Component } from 'react';

export class Home extends Component {
  static displayName = Home.name;

  render() {
    return (
      <div>
        <h1>Фронт работает на React</h1>
        <p>Помимо этого добавлен Bootstrap</p>
      </div>
    );
  }
}
